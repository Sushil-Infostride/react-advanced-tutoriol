import './App.css';
import { useState } from 'react';

const Button =(props)=>{
  const [counter,setcounter]=useState(0);
  return <div style=
  {{
    color:props.color,
     fontSize:props.size,
     textDecoration:props.underline?'underline':undefined
  }} 
  onClick={()=>setcounter((x)=>x+props.increment)}>{props.table}= {counter}</div>
}

function App() {
  return (
    <div className="App">
      <header className="App-header">
          <Button  increment={2} table={2}  underline={true} color="yellow" size="30px" />
          <Button  increment={5} table={5} underline={true} color="green" size="35px" />
          <Button  increment={17} table={17} underline={false} color="red" size="40px" />
      </header>
    </div>
  );
}

export default App;
