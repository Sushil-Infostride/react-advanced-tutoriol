import React, { useState } from 'react'


const Checkbox=({ children })=>{
    const [checked,setCheck]=useState(false);
    const NewChildren= React.Children.map(children,(child)=>{
        const clone=React.cloneElement(child,{
            size:"40px",
            color:"red",
            checked,
            setCheck
        })
        return clone;
    })

     return NewChildren;
}

const CheckBoxInput=({checked,setCheck})=>{

    return <input type="checkbox" checked={checked} onChange={(e)=>{setCheck(e.target.checked)}}></input>
}

const Label=({ children,size,color,setCheck,checked })=>{
    return <label for="test" style={{color:color, fontSize:size}} onClick={()=>{
        setCheck((!checked))
    }}>{ children } </label>
}
export default function Fifth() {
    return <Checkbox>
        <CheckBoxInput />
        <Label>CheckBox label</Label>
    </Checkbox>
}


/* 
  Compound Component
  Lets link them
*/