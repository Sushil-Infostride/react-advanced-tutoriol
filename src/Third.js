import './App.css';
import { useReducer, useState } from 'react';

function reducer({state}){
    switch(state){
        case 'Pressed_once':
        return {
            state:'Pressed_twice'
        }
        case 'Pressed_twice':
        return {
            state:'Pressed_thrice'
        }
        case 'Pressed_thrice':
        return {
            state:'Pressed_once'
        }
    }
}



const Button =(props)=>{
  const [counter,setcounter]=useState(0);

  const [state,dispatch]=useReducer(reducer,{
      state:'Pressed_once'
  })

  return (
  <>
  <div  style=
  {{
    color:props.color,
     fontSize:props.size,
     textDecoration:props.underline?'underline':undefined,
     background:props.background,
     border:props.border,
     padding:props.padding,
     borderRadius:props.radius
  }} 
   onClick={()=>dispatch()}>State Machine Example</div>
  <div>{state.state}</div>
  </>
  )
  
}

function Third() {
    const props={
        border:"2px",
        radius:"10px",
        padding:"5px",
        color:"red"
        
    }
    /* 
      @@@@ State Machine @@@@
    */

    
  return (
    <div className="App">
      <header className="App-header">
          <Button {...props} background="white"  increment={2} table={2} underline={true} color="purple" size="30px" />
          <Button {...props} background="white"  increment={5} table={5} underline={true} color="green" size="35px" />
          <Button {...props} background="white"  increment={17} table={17} underline={false} color="red" size="40px" />
      </header>
    </div>
  );
}

export default Third;
