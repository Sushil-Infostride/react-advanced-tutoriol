import './App.css';
import { useEffect, useReducer, useState } from 'react';


function Fourth() {
    const [state,setupdate]=useState('idle')
    const [data,setdata]=useState([]);

/*   
useEffect(() => {
        setupdate('loading')
        fetch('/data.json').then(()=>{
            // do something
          setupdate('loaded')
        }).catch((err)=>{
            setupdate('error')
        })
    },[]) 
*/

   function clicked(){
    setupdate('loading')
    fetch('https://jsonplaceholder.typicode.com/posts/1',{
        method:"GET"
    })
      .then((res)=>{
        try{
            console.log((res.json()))
            setupdate('loaded')
        }catch(err){
            setupdate('JSON-eror')
        }
        
    }).catch((err)=>{
        setupdate('error')
    })
}

 if (state==='error'){
     return <h1>Network Failure</h1>
 }

 if (state==='JSON-eror'){
    return <h1>Error Parsing Data</h1>
 }

 if (state==='loading'){
    return <h1>Loading</h1>
 }

if (state==='loaded'){
    return <h1>{data}</h1>
 }
    /* 
      @@@@ State Machine @@@@
    */
    
  return (
    <div className="App">
      <header className="App-header">
      <div onClick={clicked}>Current State :  {state}</div>
      </header>
    </div>
  );
}

export default Fourth;
