import './App.css';
import { useState } from 'react';

const Button =(props)=>{
  const [counter,setcounter]=useState(0);
  return <div style=
  {{
    color:props.color,
     fontSize:props.size,
     textDecoration:props.underline?'underline':undefined,
     background:props.background,
     border:props.border,
     padding:props.padding,
     borderRadius:props.radius
  }} 
  onClick={()=>setcounter((x)=>x+props.increment)}>{props.table}= {counter}</div>
}

function Second() {
    const props={
        border:"2px",
        radius:"10px",
        padding:"5px",
        color:"red"
        
    }
    /* 
      ...props Explanation :: 
         JSX code is get converted  to 
         React.createElement('Button',{
             border:"2px"
             ....
             ..
             ..
         })
         Therefore is Equivalent to <Button border="2px" />
    */
    /* 
     Destructuring of objects in javascript
      const obj={
          x:1
      }
      const obj1={
          x:1
          ...obj     -> Destructuring then value of obj x gets updated
      }
      

      :: *** ...props is usefull when shared properties are availaible :: ***
    */

    
  return (
    <div className="App">
      <header className="App-header">
          <Button {...props} background="white"  increment={2} table={2} underline={true} color="purple" size="30px" />
          <Button {...props} background="white"  increment={5} table={5} underline={true} color="green" size="35px" />
          <Button {...props} background="white"  increment={17} table={17} underline={false} color="red" size="40px" />
      </header>
    </div>
  );
}

export default Second;
